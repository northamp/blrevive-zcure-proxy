from flask import Flask, request, Response
import logging
import requests
import argparse

from zcure_message import Message

app = Flask(__name__)

class ZCureInfo:
    """ZCure server infos"""
    address: str
    port: int
    
    def __init__(self, address: str, port: int) -> None:
        self.address = address
        self.port = port

logger = logging.getLogger(__name__)

@app.route('/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'])
@app.route('/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH'])
def proxy(path):
    headers = dict(request.headers)
    content = request.get_data()

    if not app.action == "nothing":
        headers, content = handle_communication(path, headers, content, app.action, True)

    url = f"http://{app.zcure.address}:{app.zcure.port}/{path}"
    response = requests.request(
        method=request.method,
        url=url,
        headers=headers,
        data=content,
        params=request.args,
    )

    if not app.action == "nothing":
        new_response_headers, new_response_content = handle_communication(path, response.headers.items(), response.content, app.action, False)
        return Response(response=new_response_content, status=response.status_code, headers=new_response_headers)
    else:
        return Response(response=response.content, status=response.status_code, headers=response.headers.items())

def decrypt_content(content):
    if content:
        try:
            decrypted_content = Message.decrypt(content)
            logger.debug("type: {}, sequence: {}, body: {}".format(decrypted_content.message_type, decrypted_content.sequence_num, decrypted_content.body))
        except Exception as e:
            logger.warning("Failed to decrypt message: %s", e)
            return False
        else:
            return decrypted_content

def handle_communication(path: str, headers: dict, content: bytes, action: str = "print", request: bool = False):
    decrypted_content = decrypt_content(content)
    if not decrypted_content:
        return headers, content

    if action == "print":
        try:
            body = decrypted_content.body.decode()
        except UnicodeDecodeError:
            body = decrypted_content.body
        logger.info("Got the following query: {} with sequence number {} and of content {}".format(path, decrypted_content.sequence_num, body))

    if action == "add_localhost":
        if not request and path == "ZCure/V1/Matchmaking/List" and decrypted_content.message_type == 21:
            logger.info("Intercepted server list response")
            try:
                body = decrypted_content.body.decode()
            except UnicodeDecodeError:
                logger.error("Failed to decode server list response")
                return headers, content
            
            body = body + ":5:127.0.0.1:7777:0:16:false::MapIndex=-1,GameIndex=1,GoalScore=1,GameVersion=302,ServerName=localhost,ServerID=999,OwnerName=,PlaylistName=localhost"
            return headers, Message(21, decrypted_content.sequence_num, body).encrypt()

    return headers, content

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Blacklight: Retribution Revive ZCure server local query hijacker')
    parser.add_argument('-b', '--address', default="127.0.0.1", help="IP the server should bind to")
    parser.add_argument('-p', '--port', default="80", help="Port the server should bind to")

    parser.add_argument('-zd', '--zcure-address', default="209.16.144.247", help="ZCure server's IP address")
    parser.add_argument('-zp', '--zcure-port', default="80", help="Port the ZCure server listens on")

    parser.add_argument('-v', '--verbose', action='store_true', help="Print more verbose output")
    parser.add_argument('-a', '--action', default="print", help="Define the action to take on intercepted queries/replies")
    args = parser.parse_args()

    app.zcure = ZCureInfo(args.zcure_address, args.zcure_port)
    app.action = args.action

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    app.run(host=args.address, port=args.port, debug=True)
