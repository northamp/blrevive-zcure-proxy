# ZCure non-transparent proxy

Flask reverse proxy that allows inspection and on-the-fly edition of BL:RE ZCure queries (and the replies).

## Requirements

* Python3
    * Required libraries are in (`pip install -r`) `requirements.txt`

## Usage

Simply run the script, with `-h`/`--help` for additional help. By default, the reverse proxy binds to `127.0.0.1:80`.

You can change its behavior with `-a`/`--action`, and have a more verbose output with `-v`/`--verbose`. Actions are documented below.

## Actions

### print

Default, simply prints the queries/replies it processes (trying to decode them in the process).

### add_localhost

Adds `127.0.0.1:7777` to the list of available servers in the game client.
